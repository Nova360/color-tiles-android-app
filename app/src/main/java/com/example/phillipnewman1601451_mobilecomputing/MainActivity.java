package com.example.phillipnewman1601451_mobilecomputing;

        import androidx.appcompat.app.AppCompatActivity;

        import android.graphics.Color;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;

        import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Button topleftbtn;
    private Button bottomleftbtn;
    private Button toprightbtn;
    private Button middlebtn;
    private Button bottomrightbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topleftbtn = findViewById(R.id.topleftbtn);
        bottomleftbtn = findViewById(R.id.bottomleftbtn);
        toprightbtn = findViewById(R.id.toprightbtn);
        middlebtn = findViewById(R.id.middlebtn);
        bottomrightbtn = findViewById(R.id.bottomrightbtn);

        topleftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                int color = Color.argb(225,random.nextInt(256),random.nextInt(256),random.nextInt(256));

                topleftbtn.setBackgroundColor(color);
            }
        });

        bottomleftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                int color = Color.argb(225,random.nextInt(256),random.nextInt(256),random.nextInt(256));

                bottomleftbtn.setBackgroundColor(color);
            }
        });

        toprightbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                int color = Color.argb(225,random.nextInt(256),random.nextInt(256),random.nextInt(256));

                toprightbtn.setBackgroundColor(color);
            }
        });

        middlebtn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                int color = Color.argb(225,random.nextInt(256),random.nextInt(256),random.nextInt(256));

                middlebtn .setBackgroundColor(color);
            }
        });

        bottomrightbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();

                int color = Color.argb(225,random.nextInt(256),random.nextInt(256),random.nextInt(256));

                bottomrightbtn.setBackgroundColor(color);
            }
        });
    }
}
